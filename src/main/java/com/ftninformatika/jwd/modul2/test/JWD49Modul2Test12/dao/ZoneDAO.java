package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.Zona;

@Component
public class ZoneDAO {


	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	private static class ZoneRowMapper implements org.springframework.jdbc.core.RowMapper<Zona>{
		
		
		@Override
		public Zona mapRow(ResultSet rs, int rowNum) throws SQLException{
					int index = 1;
					
					
					Integer zonaId = rs.getInt(index++);
					String naziv = rs.getString(index++);
					Double cena = rs.getDouble(index++);
					Integer dozvoljeno = rs.getInt(index++);
					
					Zona zona = new Zona(zonaId, naziv, cena, dozvoljeno);
					
		
		return zona;
	}	
}
public List<Zona> 	findAll()	{
		String sql = "SELECT z.id, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima  FROM zone z ";
				
		return jdbcTemplate.query(sql, new ZoneRowMapper());
	}

public 	Zona 	findOne(Integer id)	{
	String sql = "SELECT z.id, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima  FROM zone z WHERE z.id=? ";
			
	return jdbcTemplate.queryForObject(sql, new ZoneRowMapper(),id);
}

public void save(Zona zona) {
	String sql = "INSERT INTO zone (naziv,cenaZaSat, dozvoljenoVremeParkingaUSatima) VALUES (?,?,?)";
	jdbcTemplate.update(sql, zona.getNaziv(), zona.getCenaZaSat(),zona.getDozvoljenoVremeParkingaUSatima());
}	

}
