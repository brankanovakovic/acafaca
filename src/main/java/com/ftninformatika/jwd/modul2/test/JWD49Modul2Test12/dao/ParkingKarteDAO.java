package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.swing.tree.RowMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.Zona;

@Component
public class ParkingKarteDAO {

		@Autowired
		private JdbcTemplate jdbcTemplate;
			
		private static class ParkingKarteRowMapper implements org.springframework.jdbc.core.RowMapper<ParkingKarta>{
			
			
			@Override
			public ParkingKarta mapRow(ResultSet rs, int rowNum) throws SQLException{
						int index = 1;
						Integer parkingID = rs.getInt(index++);
						String registracija = rs.getString(index++);
						String pocetakParking = rs.getString(index++);
						Integer trajanjeUminutima = rs.getInt(index++);
						String osobasaInv = rs.getString(index++);
						
						Integer zonaId = rs.getInt(index++);
						String naziv = rs.getString(index++);
						Double cena = rs.getDouble(index++);
						Integer dozvoljeno = rs.getInt(index++);
						
						Zona zona = new Zona(zonaId, naziv, cena, dozvoljeno);
						ParkingKarta parkingKarta= new ParkingKarta(parkingID, registracija, pocetakParking, trajanjeUminutima, osobasaInv, zona);
			
			return parkingKarta;
		}	
		}

public List<ParkingKarta> 	find(Integer trajanjeOD, Integer trajanjeDO )	{
	String sql = "SELECT p.id, p.registracija, p.pocetakParkinga, p.trajanjeUMinutima, p.osobaSaInvaliditetom, z.id, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima FROM parkingkarte p "+
			 "LEFT JOIN zone z ON p.zonaId=z.id WHERE p.trajanjeUMinutima>= ? AND p.trajanjeUMinutima <= ? "
			+ "ORDER BY p.id ";
	return jdbcTemplate.query(sql, new ParkingKarteRowMapper(), trajanjeOD, trajanjeDO);
}
public List<ParkingKarta> 	findAll()	{
	String sql = "SELECT p.id, p.registracija,  p.pocetakParkinga, p.trajanjeUMinutima, p.osobaSaInvaliditetom, z.id, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima  FROM parkingkarte p "+
			 "LEFT JOIN zone z ON p.zonaId=z.id "
			+ "ORDER BY p.id ";
	return jdbcTemplate.query(sql, new ParkingKarteRowMapper());
}
public ParkingKarta 	findOne(Integer id )	{
	String sql = "SELECT p.id, p.registracija, p.pocetakParkinga, p.trajanjeUMinutima, p.osobaSaInvaliditetom, z.id, z.naziv, z.cenaZaSat, z.dozvoljenoVremeParkingaUSatima FROM parkingkarte p "+
			 "LEFT JOIN zone z ON p.zonaId=z.id WHERE p.id= ? ";
			
	return jdbcTemplate.queryForObject(sql, new ParkingKarteRowMapper(), id);
}
public int update(ParkingKarta parkingKarta) {
	String sql = "UPDATE parkingkarte SET registracija = ?, pocetakParkinga=? , trajanjeUMinutima=?, osobaSaInvaliditetom=? , zonaId=?  WHERE id = ?";
	return jdbcTemplate.update(sql, parkingKarta.getRegistracija(), parkingKarta.getPocetakParkinga(),parkingKarta.getTrajanjeUMinutima(),
			parkingKarta.getOsobaSaInvaliditetom(),parkingKarta.getZona().getId(), parkingKarta.getId());
}
public void delete(Integer id) {
	String sql = "DELETE FROM parkingkarte WHERE id= ?";
	jdbcTemplate.update(sql, id);
}


}
