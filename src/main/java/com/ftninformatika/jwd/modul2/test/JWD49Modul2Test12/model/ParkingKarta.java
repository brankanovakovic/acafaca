package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model;

public class ParkingKarta {
	
	private int id;  
	private String  registracija;  
    private String pocetakParkinga ; 
	private int trajanjeUMinutima; 
	private String osobaSaInvaliditetom; 
	private Zona zona;
	
	
	public ParkingKarta(int id, String registracija, String pocetakParkinga, int trajanjeUMinutima,
			String osobaSaInvaliditetom, Zona zona) {
		super();
		this.id = id;
		this.registracija = registracija;
		this.pocetakParkinga = pocetakParkinga;
		this.trajanjeUMinutima = trajanjeUMinutima;
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
		this.zona = zona;
	}
	public ParkingKarta(String registracija, String pocetakParkinga, int trajanjeUMinutima, String osobaSaInvaliditetom,
			Zona zona) {
		super();
		
		this.registracija = registracija;
		this.pocetakParkinga = pocetakParkinga;
		this.trajanjeUMinutima = trajanjeUMinutima;
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
		this.zona = zona;
	}
	public ParkingKarta() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistracija() {
		return registracija;
	}
	public void setRegistracija(String registracija) {
		this.registracija = registracija;
	}
	public String getPocetakParkinga() {
		return pocetakParkinga;
	}
	public void setPocetakParkinga(String pocetakParkinga) {
		this.pocetakParkinga = pocetakParkinga;
	}
	public int getTrajanjeUMinutima() {
		return trajanjeUMinutima;
	}
	public void setTrajanjeUMinutima(int trajanjeUMinutima) {
		this.trajanjeUMinutima = trajanjeUMinutima;
	}
	public String getOsobaSaInvaliditetom() {
		return osobaSaInvaliditetom;
	}
	public void setOsobaSaInvaliditetom(String osobaSaInvaliditetom) {
		this.osobaSaInvaliditetom = osobaSaInvaliditetom;
	}
	public Zona getZona() {
		return zona;
	}
	public void setZona(Zona zona) {
		this.zona = zona;
	}
	@Override
	public String toString() {
		return "ParkingKarta [id=" + id + ", registracija=" + registracija + ", pocetakParkinga=" + pocetakParkinga
				+ ", trajanjeUMinutima=" + trajanjeUMinutima + ", osobaSaInvaliditetom=" + osobaSaInvaliditetom
				+ ", zona=" + zona.getNaziv() + "]";
	}
	
	

}
