package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletConfigAware;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;


import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.Zona;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.ParkingKartaService;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.ZoneService;

@Controller
@RequestMapping(value="")
public class ParkingKarteController implements ServletContextAware{
	public static final String ZONE_KEY = "zone";
	public static final String ParkingKarta_KEY = "ParkingKarta";
	public static final String Korisnik_KEY = "korisnik";
	@Autowired
	private ServletContext servletContext;
	private  String bURL;
	
	@Autowired
	private ParkingKartaService parkingKartaService;

	@Autowired
	private ZoneService zoneService;
	
	@Override
	public void setServletContext(ServletContext serlvetContext) {
		this.servletContext= serlvetContext;
		
		bURL= serlvetContext.getContextPath()+"/";
		
	}
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		//Specify the base URL for all relative URLs in a document
		bURL = servletContext.getContextPath()+"/";			
		HashMap<String, Zona> zone = new HashMap<String, Zona>();
		zone.put("bela", new Zona(1, "bela", 35.0, 1));
		zone.put("plava", new Zona(2, "plava", 44.0, 0));
		zone.put("crvena", new Zona(3, "crvena", 53.0, 2));
		HashMap<Integer, ParkingKarta> parkingKarte = new HashMap<Integer, ParkingKarta>();
		HashMap<Integer, ParkingKarta> korisnik = new HashMap<Integer, ParkingKarta>();
		parkingKarte.put(1, new ParkingKarta(1,"SO 123 456", "2019-12-18 11:30:00", 130, "DA", zone.get("bela")));
		
		parkingKarte.put(2, new ParkingKarta(2,"NS 222 333", "2019-12-17 15:00:00", 45, "NE", zone.get("plava")));
		
		parkingKarte.put(3, new ParkingKarta(3,"BG 444 555", "2019-12-16 19:00:00", 90, "NE", zone.get("crvena")));
		
		servletContext.setAttribute(ParkingKarteController.ZONE_KEY,zone);	
		servletContext.setAttribute(ParkingKarteController.ParkingKarta_KEY, parkingKarte);
		servletContext.setAttribute(ParkingKarteController.Korisnik_KEY, korisnik);
	}
	@PostMapping(value="/Create")
	public void create(@RequestParam String registracija, 
    @RequestParam String pocetakParkinga, 
	@RequestParam Integer trajanjeUMinutima, 
	@RequestParam String osobaSaInvaliditetom, 
	@RequestParam String zonaBoja, HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
	 @SuppressWarnings("unchecked")
	HashMap<String, Zona> zone =  (HashMap<String, Zona>)servletContext.getAttribute(ParkingKarteController.ZONE_KEY);
	 HashMap<Integer, ParkingKarta> parkingKarte = (HashMap<Integer,ParkingKarta>)servletContext.getAttribute(ParkingKarteController.ParkingKarta_KEY);
	 HashMap<Integer, ParkingKarta> korisnikDodate = (HashMap<Integer,ParkingKarta>)servletContext.getAttribute(ParkingKarteController.Korisnik_KEY);
	 		
		
		
	 	int brojCrvenih = 0;
	 	int nextID = 0;
	 	
		ParkingKarta parkingKarta = new ParkingKarta(registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zone.get(zonaBoja));
		if (parkingKarta!=null){
		nextID = parkingKarte.size();
		nextID++;
		parkingKarta.setId(nextID);
		parkingKarte.put(parkingKarta.getId(),parkingKarta);
		korisnikDodate.put(parkingKarta.getId(), parkingKarta);
		}
		

		for(ParkingKarta p : parkingKarte.values() ) {
			System.out.println(p.toString());
			if(p.getZona().getNaziv().equals("crvena")) {
				brojCrvenih++;
				
			}
		}
		System.out.println("Broj crvenih zona je" + brojCrvenih);
		response.sendRedirect(bURL+"ParkingKarte/Zadatak3");
}
	@GetMapping(value="ParkingKarte/Zadatak3")
	public ModelAndView index( HttpSession session, HttpServletResponse response) throws IOException {
		 @SuppressWarnings("unchecked")
		HashMap<Integer, ParkingKarta> parkingKarte = (HashMap<Integer,ParkingKarta>)servletContext.getAttribute(ParkingKarteController.ParkingKarta_KEY);
		 HashMap<Integer, ParkingKarta> korisnikDodate = (HashMap<Integer,ParkingKarta>)servletContext.getAttribute(ParkingKarteController.Korisnik_KEY);
		 List<ParkingKarta> parkinzi = parkingKartaService.findAll();
			int ukupniSati= 0;
			double ukupnaCena= 0;
			for(ParkingKarta p: parkinzi) {
				if(p.getOsobaSaInvaliditetom().equalsIgnoreCase("NE")) {
			ukupniSati=p.getTrajanjeUMinutima()/60;
				
				 if(p.getTrajanjeUMinutima()%60 != 0) {
					 ukupniSati+=1;
				 }
			ukupnaCena+= (double)ukupniSati*p.getZona().getCenaZaSat() ;
				}}
			for(ParkingKarta p: korisnikDodate.values()) {
				System.out.println("Dodata parking karta "+ p.toString());
			}
			
			ModelAndView rezultat = new ModelAndView("zadatak3");
			rezultat.addObject("parkinzi", parkinzi);
			rezultat.addObject("ukupnaCena",ukupnaCena);
			return rezultat;
		
	}
	@GetMapping(value="ParkingKarte/Details")
	public ModelAndView zadatak4(HttpSession session, HttpServletResponse response,
			@RequestParam Integer id) throws IOException {
			ParkingKarta parkingKarta = parkingKartaService.findOne(id);
			List<Zona> zone = zoneService.findAll();
			
			ModelAndView rezultat = new ModelAndView("zadatak4"); // naziv template-a
			rezultat.addObject("parkingKarta", parkingKarta); // podatak koji se šalje template-u
			rezultat.addObject("zone", zone); // podatak koji se šalje template-u

			return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
		
	}
	@PostMapping(value="Delete")
	public void delete(HttpSession session, HttpServletResponse response,
			@RequestParam Integer id) throws IOException {
			
		parkingKartaService.delete(id);;
		
		response.sendRedirect(bURL+"ParkingKarte/Zadatak3");
	}
		
	
	
	
	@GetMapping(value="ParkingKarte/Zadatak5")
	public ModelAndView zadatak5( HttpSession session, HttpServletResponse response,
			@RequestParam(required=false, defaultValue="0") Integer trajanjeOD, 
			@RequestParam(required=false, defaultValue="240") Integer trajanjeDO) throws IOException {
		 @SuppressWarnings("unchecked")
		HashMap<Integer, ParkingKarta> parkingKarte = (HashMap<Integer,ParkingKarta>)servletContext.getAttribute(ParkingKarteController.ParkingKarta_KEY);
		 HashMap<Integer, ParkingKarta> korisnikDodate = (HashMap<Integer,ParkingKarta>)servletContext.getAttribute(ParkingKarteController.Korisnik_KEY);
		 List<ParkingKarta> parkinzi = parkingKartaService.find(trajanjeOD, trajanjeDO);
			
		 List<ParkingKarta> parkinzi2 = parkingKartaService.find(60, 120);
		 for (ParkingKarta parkingKarta : parkinzi2) {
			System.out.println(parkingKarta.toString());
		}
			ModelAndView rezultat = new ModelAndView("zadatak5");
			rezultat.addObject("parkinzi", parkinzi);
			
			return rezultat;
		
	}
	
	@PostMapping(value="Edit")
	public String edit(@RequestParam Integer id,
			@RequestParam String registracija, 
			@RequestParam String pocetakParkinga, 
			@RequestParam Integer trajanjeUMinutima, 
			@RequestParam String osobaSaInvaliditetom, 
			@RequestParam Integer zonaId, HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
	 ParkingKarta parkingKarta = parkingKartaService.findOne(id); 
	 
	 if(parkingKarta==null) {
		 return "redirect:ParkingKarte/Zadatak3";
	 }
	if(registracija=="" || pocetakParkinga=="" || osobaSaInvaliditetom=="" || zonaId==0 || trajanjeUMinutima<15) {
		response.sendRedirect(bURL+"ParkingKarte/Details?id="+ id);
	}
	 		Zona zona = zoneService.findOne(zonaId);
	
	parkingKarta.setRegistracija(registracija);
	parkingKarta.setPocetakParkinga(pocetakParkinga);
	parkingKarta.setTrajanjeUMinutima(trajanjeUMinutima);
	parkingKarta.setOsobaSaInvaliditetom(osobaSaInvaliditetom);
	parkingKarta.setZona(zona);
	
	parkingKartaService.update(parkingKarta);
	
	return "redirect:ParkingKarte/Zadatak3";
	}
	
}