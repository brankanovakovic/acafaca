package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service;

import java.util.List;


import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.Zona;

public interface ZoneService {

	List<Zona> findAll();
	Zona save(Zona zona);
	Zona findOne(Integer id);
}
	