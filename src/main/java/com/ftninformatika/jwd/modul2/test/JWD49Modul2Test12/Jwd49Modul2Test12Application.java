package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;



@SpringBootApplication
public class Jwd49Modul2Test12Application {
	
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Jwd49Modul2Test12Application.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(Jwd49Modul2Test12Application.class, args);
	}

}
