package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service;

import java.util.List;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;

public interface ParkingKartaService {

		List<ParkingKarta> find(Integer trajanjeOD, Integer trajanjeDO);
		List<ParkingKarta> findAll();
		ParkingKarta findOne(Integer id);
		int update(ParkingKarta parkingKarta);
		void delete(Integer id);
}
