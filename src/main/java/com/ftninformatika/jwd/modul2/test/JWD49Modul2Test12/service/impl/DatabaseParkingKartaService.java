package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.dao.ParkingKarteDAO;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.ParkingKartaService;

@Service
public class DatabaseParkingKartaService implements ParkingKartaService {

	@Autowired ParkingKarteDAO dao;
	
	@Override
	public List<ParkingKarta> find(Integer trajanjeOD, Integer trajanjeDO){
		List<ParkingKarta> parkingKarte = dao.find(trajanjeOD, trajanjeDO);
		return parkingKarte;
		
	}

	@Override
	public List<ParkingKarta> findAll() {
		List<ParkingKarta> parkingKarte = dao.findAll();
		return parkingKarte;
	}

	@Override
	public ParkingKarta findOne(Integer id) {
		ParkingKarta parkingKarta = dao.findOne(id);
		return parkingKarta;
	}

	@Override
	public int update(ParkingKarta parkingKarta) {
			return	dao.update(parkingKarta);
	
	}

	@Override
	public void delete(Integer id) {
		dao.delete(id);
	}
}
