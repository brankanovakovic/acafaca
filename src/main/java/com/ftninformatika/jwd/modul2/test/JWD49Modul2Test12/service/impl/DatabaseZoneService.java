package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.dao.ZoneDAO;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.ParkingKarta;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.Zona;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.ZoneService;

@Service
public class DatabaseZoneService implements ZoneService {

		@Autowired ZoneDAO zoneDAO;

		@Override
		public List<Zona> findAll() {
		List<Zona> zone = zoneDAO.findAll();
		return zone;
		}

		@Override
		public Zona save(Zona zona) {
			zoneDAO.save(zona);
			return zona;
		}

		@Override
		public Zona findOne(Integer id) {
			Zona zona = zoneDAO.findOne(id);
			return zona;
		}
		
		
		
}
