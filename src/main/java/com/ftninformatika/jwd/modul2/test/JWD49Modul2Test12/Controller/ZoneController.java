package com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.Controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.model.Zona;
import com.ftninformatika.jwd.modul2.test.JWD49Modul2Test12.service.ZoneService;

@Controller
@RequestMapping(value="/Zone")
public class ZoneController {

	@Autowired
	private ZoneService zoneService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	@GetMapping
	@ResponseBody
	public Map<String, Object> index(@RequestParam(required=false, defaultValue="") String naziv,
			@RequestParam(required=false, defaultValue="") Double cena,
			@RequestParam(required=false, defaultValue="") Integer vreme) {
		// čitanje
		List<Zona> zone = zoneService.findAll();

		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("zone", zone);
		for(Zona z :zone) {
			System.out.println(z.toString());
		}
		return odgovor;
	}
	
	@GetMapping(value="baseURL")
	@ResponseBody
	public Map<String, Object> baseURL() {
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("baseURL", baseURL);	
		return odgovor;
	}
	@PostMapping(value="/Create")
	@ResponseBody
	public Map<String, Object> create(@RequestParam String naziv, 
			@RequestParam(required=false, defaultValue="") Double cena,
			@RequestParam(required=false, defaultValue="") Integer vreme,
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		
	
		// validacija
		if (naziv.equals("")) {
			Map<String, Object> odgovor = new LinkedHashMap<>();
			odgovor.put("status", "greska");
			odgovor.put("poruka", "Naziv ne sme biti prazan!");
			return odgovor;
		}

		Zona zona= new Zona(naziv, cena, vreme);
		
		zoneService.save(zona);
		
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		return odgovor;
	}
	
}
