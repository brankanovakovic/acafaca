var baseURL = ""

function popuniBaseURL() {
	// traži od servera baseURL
	$.get("Zone/baseURL", function(odgovor) { // GET zahtev
		console.log(odgovor)

		if (odgovor.status == "ok") {
			baseURL = odgovor.baseURL // inicjalizuj globalnu promenljivu baseURL
			$("base").attr("href", baseURL) // postavi href atribut base elementa
		}
	})
	console.log("GET: " + "baseURL")
}
function popuniZone() {
	var tabela = $("table.tabela")
	var nazivInput = $("input[name=naziv]")
	var cenaInput =  $("input[name=cena]")
	var vremeInput =  $("input[name=vreme]")
	var naziv = nazivInput.val()
	var cena = cenaInput.val()
	var vreme= vremeInput.val()

	var params = {
		naziv: naziv,
		cena: cena,
		vreme:vreme
	}
	console.log(params)
	$.get("Zone", params, function(odgovor) {
		console.log(odgovor)

		if (odgovor.status == "ok") {
			tabela.find("tr:gt(1)").remove()
			
			var zone = odgovor.zone
			for (var it in zone) {
				tabela.append( 	
		'<tr>'+
			'<td>'+ zone[it].naziv +'</td>'+
			'<td> '+ zone[it].cenaZaSat+'</td>'+
			'<td>  '+ zone[it].cenaZaSat*1.18+'</td>'+
			'<td> '+ zone[it].dozvoljenoVremeParkingaUSatima+'</td>'+
		'</tr>'
					
				)
			}
		}
	})
	
}
function dodajZona() {
	var nazivInput = $("input[name=naziv]")
	var cenaInput =  $("input[name=cena]")
	var vremeInput =  $("input[name=vreme]")
	var naziv = nazivInput.val()
	var cena = cenaInput.val()
	var vreme= vremeInput.val()

	var params = {
		naziv: naziv,
		cena: cena,
		vreme:vreme
	}
	console.log(params)
	$.post("Zone/Create", params, function(odgovor) {
		console.log(odgovor)
		window.location.replace("zadatak7.html")
		if (odgovor.status == "ok") {
		} else if (odgovor.status == "neovlascen") {
			window.location.replace("zanrovi.html")
		} else if (odgovor.status == "greska") {
			$("p.greska").text(odgovor.poruka)
		}
	})
	console.log("POST: Zone/Create")
}


$(document).ready(function() {
	popuniBaseURL() 
	popuniZone()
	$("form").submit(function() {
		dodajZona()
	return false

})
})