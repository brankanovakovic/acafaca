DROP SCHEMA IF EXISTS parking;
CREATE SCHEMA parking DEFAULT CHARACTER SET utf8;
USE parking;

CREATE TABLE zone (
	id INT AUTO_INCREMENT, 
	naziv VARCHAR(45) NOT NULL, 
	cenaZaSat DECIMAL(10, 2) NOT NULL, 
    dozvoljenoVremeParkingaUSatima INT NULL, 
	PRIMARY KEY(id)
);

CREATE TABLE parkingKarte (
	id BIGINT AUTO_INCREMENT, 
	registracija VARCHAR(50) NOT NULL UNIQUE, 
    pocetakParkinga VARCHAR(50) NOT NULL, 
	trajanjeUMinutima INT NOT NULL, 
	osobaSaInvaliditetom ENUM('DA', 'NE') NOT NULL, 
	zonaId INT NOT NULL, 
	PRIMARY KEY(id), 
    INDEX(registracija ASC), 
    INDEX(zonaId ASC), 
	FOREIGN KEY(zonaId) REFERENCES zone(id)
		ON DELETE RESTRICT
);

INSERT INTO zone (id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima) VALUES (1, 'bela', 35.0, NULL);
INSERT INTO zone (id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima) VALUES (2, 'plava', 44.0, NULL);
INSERT INTO zone (id, naziv, cenaZaSat, dozvoljenoVremeParkingaUSatima) VALUES (3, 'crvena', 53.0, 2);

INSERT INTO parkingKarte (registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zonaId) VALUES ('SO 123 456', '2019-12-18 11:30:00', 130, 'DA', 3);
INSERT INTO parkingKarte (registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zonaId) VALUES ('NS 222 333', '2019-12-17 15:00:00', 45, 'NE', 1);
INSERT INTO parkingKarte (registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zonaId) VALUES ('BG 444 555', '2019-12-16 19:00:00', 90, 'NE', 2);
INSERT INTO parkingKarte (registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zonaId) VALUES ('NS 666 777', '2019-12-16 07:45:00', 20, 'NE', 3);
INSERT INTO parkingKarte (registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zonaId) VALUES ('SU 888 999', '2019-12-17 10:20:00', 150, 'NE', 3);
INSERT INTO parkingKarte (registracija, pocetakParkinga, trajanjeUMinutima, osobaSaInvaliditetom, zonaId) VALUES ('BG 111 222', '2019-12-18 11:00:00', 150, 'DA', 2);
